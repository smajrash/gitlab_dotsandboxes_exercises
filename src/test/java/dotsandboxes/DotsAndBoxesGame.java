public class DotsAndBoxesGame {
    private boolean[][] horizontalLines;
    private boolean[][] verticalLines;
    private int gridSize;


    public DotsAndBoxesGame(int gridSize) {
        this.gridSize = gridSize;
        horizontalLines = new boolean[gridSize + 1][gridSize];
        verticalLines = new boolean[gridSize][gridSize + 1];
    }

    public boolean drawHorizontalLine(int row, int col) {
        if (row >= 0 && row < gridSize + 1 && col >= 0 && col < gridSize && !horizontalLines[row][col]) {
            horizontalLines[row][col] = true;
            return true;
        }
        return false;
    }

    public boolean drawVerticalLine(int row, int col) {
        if (row >= 0 && row < gridSize && col >= 0 && col < gridSize + 1 && !verticalLines[row][col]) {
            verticalLines[row][col] = true;
            return true;
        }
        return false;
    }

    public boolean isLineDrawn(int row, int col, LineType type) {
        if (type == LineType.HORIZONTAL) {
            return horizontalLines[row][col];
        } else if (type == LineType.VERTICAL) {
            return verticalLines[row][col];
        }
        return false;
    }

    public boolean isSquareComplete(int row, int col) {
        return isLineDrawn(row, col, LineType.HORIZONTAL) &&
               isLineDrawn(row + 1, col, LineType.HORIZONTAL) &&
               isLineDrawn(row, col, LineType.VERTICAL) &&
               isLineDrawn(row, col + 1, LineType.VERTICAL);
    }
}

enum LineType {
    HORIZONTAL, VERTICAL
}
