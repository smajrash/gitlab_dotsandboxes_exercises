import org.junit.Test;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DotsAndBoxesGameTest {

    @Test
    public void testSquareComplete() {
        // Given
        DotsAndBoxesGame game = new DotsAndBoxesGame(2);
        // Assuming the following lines form a complete square
        game.drawHorizontalLine(0, 0);
        game.drawHorizontalLine(0, 1);
        game.drawVerticalLine(0, 0);
        game.drawVerticalLine(1, 0);

        // When
        boolean squareComplete = game.isSquareComplete(0, 0);

        // Then
        assertTrue(squareComplete); // The test should pass if the square is correctly recognized as complete
    }
}
